class NumbersController < ApplicationController
  before_action :setup

  def index
    @numbers = (@offset + 1..@offset + @quantity).to_a
  end

  private

  def setup
    @page = (params[:page] || 0).to_i
    @quantity = (params[:quantity] || 1000).to_i
    if 1000000 % @quantity == 0
      @last_page = 1000000/@quantity - 1
    else
      @last_page = 1000000/@quantity
    end
    #@page = @last_page if @page > @last_page
    @quantity = 9999 if @quantity > 9999
    @offset = @page * @quantity
  end
end
