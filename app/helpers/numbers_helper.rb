module NumbersHelper

  def to_word(num)
    result = []
    digits = num.to_s.rjust(7, '0')
    result.push(process_numbers(digits[-3..-1], nil))
    result.push(process_numbers(digits[-6..-4], 1000))
    result.push(process_numbers(digits[-7], 1000000))
    result = result.reverse.join(" ")
    return result.split(' ')[1..-1].join(" ") if result.split(' ')[0] == "and"
    result
  end

  def process_numbers(digits, order)
    num_array = { 1 => "one", 2 => "two", 3 => "three", 4 => "four", 5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine", 10 => "ten",
      11 => "eleven", 12 => "tweve", 13 => "thirteen", 14 => "fourteen", 15 => "fifteen", 16 => "sixteen", 17 => "seventeen", 18 => "eighteen",
      19 => "nineteen", 20 => "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
      90 => "ninety", 100 => "hundred", 1000 => "thousand", 1000000 => "million"
    }
    num = digits.to_i
    array = []
    if num <= 99 && num_array[num]
      array.push("and") unless order || digits[0] != "0" || digits == "000"
      array.push(num_array[num], num_array[order])
    elsif num <= 99
      array.push("and") unless order || digits[0] != "0" || digits == "000"
      array.push(num_array[num.to_s[0].to_i*10], num_array[num.to_s[-1].to_i])
      array.push(num_array[order]) if num != 0
    else
      array.push(num_array[num.to_s[0].to_i], num_array[size(num)])
      array.push("and") unless num_array[num] || num.to_s[-2..-1] == "00"
      array.push(process_numbers(num % 100, nil))
      array.push(num_array[order]) if order
    end
    array.join(" ")
  end

  def size(num)
    10**(num.digits.count - 1)
  end

  def next_page_links
    if @page + 3 >= @last_page
      max = @last_page -1
    else
      max = @page + 2
    end
    (@page + 1..max).to_a
  end

  def previous_page_links
    if @page - 2 < 2
      min = 1
    else
      min = @page - 2
    end
    (min..@page - 1).to_a
  end
end
