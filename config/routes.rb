Rails.application.routes.draw do
  root 'numbers#index'

  get '/numbers' => 'numbers#index'
  post '/numbers' => 'numbers#index'
end
