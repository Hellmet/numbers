require 'rails_helper'

feature "print numbers as words and paginate" do
  before :each do
    visit root_path
  end

  scenario "numbers are printed properly on index" do
    expect(page).to have_text("one")
    expect(page).to have_text("fifty one")
    expect(page).to have_text("one hundred and fifty six")
  end

  scenario "changing number of results works correctly" do
    fill_in "Result per page", with: 50
    click_on "Save"
    expect(page).to have_text("one")
    expect(page).to have_text("fifty")
    expect(page).to_not have_text("fifty one")
  end

  scenario "Last page changes according to the amount of results on the page" do
    within ".pagination" do
      expect(page).to have_text("1000")
    end
    fill_in "Result per page", with: 50
    click_on "Save"
    within ".pagination" do
      expect(page).to have_text("20000")
    end
  end

  scenario "Next and previous buttons work" do
    expect(page).to have_text("one")
    expect(page).to_not have_text("one thousand and one")
    click_on "Next"
    expect(page).to have_text("one thousand and one")
    click_on "Previous"
    expect(page).to have_text("one")
  end

  scenario "Previous/next buttons are hidden when on first/last page" do
    expect(page).to_not have_text("Previous")
    click_on "1000"
    expect(page).to have_text("nine hundred and ninety nine thousand seven hundred and fifty three")
    expect(page).to_not have_text("Next")
  end

  scenario "Cant input ridiculous numbers for results per page (max 9999)" do
    fill_in "Result per page", with: 10100
    click_on "Save"
    expect(page).to have_text("nine thousand nine hundred and ninety nine")
    expect(page).to_not have_text("ten thousand")
    click_on "Next"
    expect(page).to have_text("ten thousand")
  end
end
